package clock;

import java.awt.Color;
import java.awt.Graphics;

/**
 * A silly view of a clock that draws bars that get longer as the time progresses, 
 * one for hour, minute, and seconds.
 * 
 * @author Chuck Cusack, 2001. Extensive refactoring, January 2013.
 */
public class BarClock extends ClockView {
	public static final int	width	= 450;
	public static final int	height	= 500;

	public BarClock(ClockInterface clock) {
		super(width, height, clock);
	}

	/*
	 * Abstract methods from ClockView.
	 */
	@Override
	public void drawFace(Graphics g) {}

	@Override
	public void drawHours(Graphics g) {
		int darkness = (int) (255 - 255 * getHours() / 12.0);
		g.setColor(new Color(darkness, 0, 0));
		g.fillRect(50, 50, 50, 25 + getHours() * 25);
	}

	@Override
	public void drawMinutes(Graphics g) {
		int darkness = (int) (255 - 255 * getMinutes() / 60.0);
		g.setColor(new Color(0, darkness, 0));
		g.fillRect(150, 50, 50, 5 + getMinutes() * 5);
	}

	@Override
	public void drawSeconds(Graphics g) {
		int darkness = (int) (255 - 255 * getSeconds() / 60.0);
		g.setColor(new Color(0, 0, darkness));
		g.fillRect(250, 50, 50, getSeconds() * 5);
	}
}
